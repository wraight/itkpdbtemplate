# ITk PDB Template

### [Streamlit](https://www.streamlit.io) (python**3**) code to upload component/test schemas for registration on production database (PDB)

**NB** Powered by [itkdb](https://pypi.org/project/itkdb/)

Check requirements file for necessary libraries

---

## WebApp Layout

**Basic Idea:**
* template for ITk webApps
* based on: [*streamlit template*](https://github.com/kwraight/streamlitTemplate)

### Main Page
  * includes *sidebar* for checking debug states and quick token renewal

### Authenticate Page
  * input PDB access codes (only stored in browser cache)

### Debug Page:
**(Broom cupboard)**
  * State settings

---

## Adding content

An additional file is supplied to make use of additional content (based on *usual* template image).
The procedure is as follows:

1. make a ''pages'' sub-directory

2. add files to directory (use prefix: ''page_'')
  * add letter suffix to order pages: e.g. ''page_A_first.py'' file --> ''first'' page of webApp

---

## Running locally

Run webApp locally:

* get required libraries:
> python3 -m pip install -r requirements

* run streamlit:
> streamlit run mainApp.py
  * option to add credentials at end with format: ''ac1:X ac2:Y''

* open browser at ''localhost:8501''

---

## Running via Docker

Either of two files can be used to build basic templates (structural files):

build *usual* image:

> docker build . -f dockerFiles/Dockerfile -t itk-app

build *Cern usable* image (for use with openShift):

> docker build . -f dockerFiles/DockerfileCern -t itk-app

An additional file is supplied to make use of additional content (based on *usual* template image).
The procedure is as follows:

* build *new* image (from ''pages'' parent directory)

> docker build . -f dockerFiles/Dockerfile -t itk-app

The build will copy files in the pages directory into the image and use these as content of the webApp.

* run container from image:

> docker run -p 8501:8501 itk-app

* open browser at ''localhost:8501''

---

## Running with mounted volume

This allows changes to files in mounted directory to be propagated to container immediately (*i.e.* without rebuilding) - useful for development!
**NB** this will overwrite any files in linked directory:

> docker run -p 8501:8501 -v $(pwd)/pages:/code/pages itk-app
