### streamlit stuff
import streamlit as st
### general stuff
import os
import sys
from datetime import datetime
###session stuff
cwd = os.getcwd()
sys.path.insert(1, cwd)
import stInfrastructure as infra
import page_debug
#pagesDir=cwd+"/PDB_code/uploadTestResultApp/pages"
# get pages from pages directory
import importlib
## basic pages
basicDir=cwd+"/basic"
basicFiles= sorted([f for f in os.listdir(basicDir) if os.path.isfile(os.path.join(basicDir, f)) and "basic_" in f and not f.endswith("~")])
## other pages
pagesDir=cwd+"/pages"
pageFiles= sorted([f for f in os.listdir(pagesDir) if os.path.isfile(os.path.join(pagesDir, f)) and "page_" in f and not f.endswith("~")])
## get modules
sys.path.insert(1, basicDir)
modules= [importlib.import_module(p[:-3]) for p in basicFiles]
sys.path.insert(1, pagesDir)
modules+= [importlib.import_module(p[:-3]) for p in pageFiles]
#infra=importlib.import_module('stInfrastructure')
DBaccess=importlib.import_module('DBaccess')

#####################
### main
#####################

def main():
    ### get state variable
    state = infra.get()

    ### define pages dictionary
    # zip names (after '_') with modules
    pages = dict(zip([p.split('_')[-1][:-3] for p in basicFiles+pageFiles],[getattr(m,"main_part") for m in modules]))
    pages['Broom cupboard']=page_debug.main_part

    ##############
    # side bar
    ##############

    ### title and credits
    st.sidebar.title(":telescope: ITk PDB WebApp")
    st.sidebar.markdown("### :zap: Powered by [itkdb](https://pypi.org/project/itkdb/) :zap:")

    st.sidebar.markdown("---")

    ### page selection
    page = st.sidebar.radio("Select page: ", tuple(pages.keys()))
    try:
        if state.debug:
            st.sidebar.markdown("Selected: "+page)
    except AttributeError:
        pass

    st.sidebar.markdown("---")

    ### renew token button
    try:
        if state.myClient:
            exTime = datetime.fromtimestamp(state.myClient.user.expires_at)
            st.sidebar.markdown("Token expires at: "+exTime.strftime("%d.%m.%Y %H:%M"))
        if state.authenticate['ac1'] and state.authenticate['ac2']:
            if st.sidebar.button("Renew Token"):
                nowTime = datetime.now()
                state.authenticate['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
                st.sidebar.markdown("Registed at: "+state.authenticate['time'])
                state.myClient=getattr(DBaccess,"AuthenticateUser")(state.authenticate['ac1'],state.authenticate['ac2'])
        else:
            st.sidebar.markdown("register on top page")
    except:
        st.sidebar.markdown("No client set")

    st.sidebar.markdown("---")

    ### inst, proj data
    if "authenticate" in [i for i in state.__dict__.keys() if i[:1] != '_']:
        try:
            state.authenticate['inst']=st.sidebar.selectbox("Institution:", state.authenticate['instList'], format_func=lambda x: x['code'], index=state.authenticate['instList'].index(state.authenticate['inst']))
        except KeyError:
            st.sidebar.markdown("No instList set")
        try:
            state.authenticate['proj']=st.sidebar.selectbox("Project:", state.authenticate['projList'], format_func=lambda x: x['code'], index=state.authenticate['projList'].index(state.authenticate['proj']))
        except KeyError:
            st.sidebar.markdown("No projList set")
    else:
        st.sidebar.markdown("lists not found")

    st.sidebar.markdown("---")

    ### mini-state summary
    if st.sidebar.button("State Summary"):
        # st.write(dir(state))
        myatts=[x for x in dir(state) if "__" not in x]
        # st.sidebar.markdown(myatts)
        for ma in myatts:
            if ma=="broom": continue
            st.sidebar.markdown(f"**{ma}** defined")

    st.sidebar.markdown("---")

    ### debug toggle
    debug = st.sidebar.checkbox("Toggle debug")
    if debug:
        state.debug=True
    else: state.debug=False

    st.sidebar.markdown("---")

    ### small print
    st.sidebar.markdown("*small print*:")
    st.sidebar.markdown("[git repository](https://gitlab.cern.ch/wraight/itkpdbtemplate)")
    st.sidebar.markdown("[docker repository](https://hub.docker.com/repository/docker/kwraight/itk-pdb-template)")

    st.sidebar.markdown("streamlitTemplate: "+infra.Version())
    st.sidebar.markdown("itkPdbTemplateTemplate: "+DBaccess.Version())
    ### display  selected page using state variable
    pages[page](state)


#########################################################################################################

#########################################################################################################

### run
if __name__ == "__main__":
    main()
