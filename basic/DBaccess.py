import streamlit as st
###
import itkdb
import itkdb.exceptions as itkX

#####################
### Things
#####################

def Version():
    return ("18-06-21")

def AuthenticateUser(ac1,ac2):
    user = itkdb.core.User(accessCode1=ac1, accessCode2=ac2)
    user.authenticate()
    client = itkdb.Client(user=user)
    return client

@st.cache(suppress_st_warning=True)
def DbGet(client, myAction, inData, listFlag=False):
    outData=None
    if listFlag:
        try:
            outData = list(client.get(myAction, json=inData ) )
        except itkX.BadRequest as b: # catch double registrations
            st.write(myAction+": went wrong for "+str(inData))
            st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    else:
        try:
            outData = client.get(myAction, json=inData)
        except itkX.BadRequest as b: # catch double registrations
            st.write(myAction+": went wrong for "+str(inData))
            st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    return outData

@st.cache(suppress_st_warning=True)
def DbPost(client, myAction, inData):
    outData=None
    try:
        outData=client.post(myAction, json=inData)
    except itkX.BadRequest as b: # catch double registrations
        st.write(myAction+": went wrong for "+str(inData))
        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
        try:
            st.write('**'+str(b)[str(b).find('"paramMap": ')+len('"paramMap": '):-8]+'**') # sucks
        except:
            pass
    except itkX.ServerError as b: # catch double registrations
        st.write(myAction+": went wrong for "+str(inData))
        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    return outData

@st.cache
def GetInstList(client):
    myList=[]
    try:
        myList = list(client.get('listInstitutions'))
    except itkX.BadRequest as b: # catch double registrations
        st.write('listInstitutions'+": went wrong for "+str(inData))
        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    return myList

@st.cache
def GetProjList(client):
    myList=[]
    try:
        myList= list(client.get('listProjects'))
    except itkX.BadRequest as b: # catch double registrations
        st.write('listProjects'+": went wrong for "+str(inData))
        st.write('**'+str(b)[str(b).find('"message": ')+len('"message": '):str(b).find('"paramMap"')-8]+'**') # sucks
    return myList
