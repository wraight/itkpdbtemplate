import streamlit as st
import stInfrastructure as infra
import datetime
import os
import sys
import json
import subprocess
import ast
### PDB stuff
import DBaccess
import itkdb

#####################
### Top page
#####################

### format datetime
def DateFormat(dt):
    return str("{0:02}-{1:02}-{2:04}".format(dt.day,dt.month,dt.year))+" at "+str("{0:02}:{1:02}".format(dt.hour,dt.minute))


def GetCodes(args):
    code1,code2=None,None
    for a in args:
        if "ac1" in a: code1=a[4::].strip('"').strip("'")
        if "ac2" in a: code2=a[4::].strip('"').strip("'")
    return code1,code2


### main part
def main_part(state):
    ### current time
    nowTime = datetime.datetime.now()
    ### get page name
    pageName=__file__.split('_')[-1].replace('.py','')
    ### title and (optional) instructions
    st.title(":ticket: "+pageName)
    st.write("""### :calendar: ("""+DateFormat(nowTime)+""")""")
    st.write(" --- ")
    ###

    # debug check
    if state.debug:
        st.write("### Debug is on")

    ### add page attribute to state
    # st.write([i for i in state.__dict__.keys() if i[:1] != '_'])
    if pageName in [i for i in state.__dict__.keys() if i[:1] != '_']:
        if state.debug: st.write("state."+pageName+" defined")
    else:
        state.__setattr__(pageName,{})

    ### getting attribute
    #st.write(state.__dir__())
    pageDict=state.__getattribute__(pageName)

    ### commandline inputs?
    # st.write([i for i in state.__dict__.keys() if i[:1] != '_'])

    # token check
    try:
        if pageDict['token']:
            st.write(":white_check_mark: Got Token")
        else:
            st.write("No user found")
    except KeyError:
        st.write("No user state set form commandline")

    if state.debug: st.write("sys args:",sys.argv)
    if len(sys.argv)>1:
        ac1,ac2=GetCodes(sys.argv)
        st.info("Read codes from commandLine")
        #st.write(ac1,",",ac2)
        if ac1==None or ac2==None:
            st.error("Problem recognising credentials. Please check arguments and try again.")
        else:
            pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
            pageDict['ac1'],pageDict['ac2']=ac1,ac2
            state.myClient=DBaccess.AuthenticateUser(pageDict['ac1'],pageDict['ac2'])
    else:
        # input passwords
        infra.TextBox(pageDict, 'ac1', 'Enter password 1', True)
        infra.TextBox(pageDict, 'ac2', 'Enter password 2', True)

        if state.debug:
            st.write("**DEBUG** tokens")
            st.write("ac1:",pageDict['ac1'],", ac2:",pageDict['ac2'])

        if st.button("Get Token"):
            pageDict['time']=str("{0:02}:{1:02}".format(nowTime.hour,nowTime.minute))
            state.myClient=DBaccess.AuthenticateUser(pageDict['ac1'],pageDict['ac2'])

    try:
        st.write("Registed at",pageDict['time'])
        exTime = datetime.datetime.fromtimestamp(state.myClient.user.expires_at)
        st.write("Token expires at: "+exTime.strftime("%d.%m.%Y %H:%M"))

        # feedback on passwords
        try:
            pageDict['user']=state.myClient.get('getUser', json={'userIdentity': state.myClient.user.identity})
            st.success("Returning token for "+str(pageDict['user']['firstName'])+" "+str(pageDict['user']['lastName'])+" seems ok. (check debug for details)")
            if state.debug:
                st.write("User (id):",pageDict['user']['firstName'],pageDict['user']['lastName'],"("+pageDict['user']['id']+")")
        except:
            st.error("Bad token registered. Please close streamlit and try again")
    except KeyError:
        st.info("No token yet registed")


    if "myClient" in list(state.__dict__.keys()):
        resetLists=False
        ### reset option
        prog_text = st.empty()
        if st.button("reset lists"):
            resetLists=True
        ### gather lists
        if "instList" not in list(pageDict.keys()) or resetLists:
            prog_text.text("** Please wait for *Institutions* to be compiled **")
            st.write("### Get *Institutions*")
            pageDict['instList']=DBaccess.GetInstList(state.myClient)
            pageDict['instList'] = sorted(pageDict['instList'], key=lambda k: k['name'])
            st.write("Done!")
            if "inst" not in pageDict.keys():
                pageDict['inst']=[x for x in pageDict['instList'] if x['code']==pageDict['user']['institutions'][0]['code']][0]
        else:
            st.write("Got institutions")
        st.write("Current setting:",pageDict['inst']['name'])

        if "projList" not in list(pageDict.keys()) or resetLists:
            prog_text.text("** Please wait for *Projects* to be compiled **")
            st.write("### Get *Projects*")
            pageDict['projList']=DBaccess.GetProjList(state.myClient)
            pageDict['projList'] = sorted(pageDict['projList'], key=lambda k: k['name'])
            st.write("Done!")
            if "proj" not in pageDict.keys():
                pageDict['proj']=[x for x in pageDict['projList'] if x['code']==pageDict['user']['preferences']['defaultProject']][0]
        else:
            st.write("Got projects")
        prog_text.text("** Lists are compiled **")
        st.write("Current setting:",pageDict['proj']['name'])

    else:
        st.write("Get info when registered")
