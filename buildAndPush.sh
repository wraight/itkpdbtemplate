docker build . -f dockerFiles/Dockerfile -t kwraight/itk-pdb-template:1.0
docker push kwraight/itk-pdb-template:1.0
docker build . -f dockerFiles/Dockerfile -t kwraight/itk-pdb-template:latest
docker push kwraight/itk-pdb-template:latest
docker build . -f dockerFiles/DockerfileCern -t kwraight/itk-pdb-template:cern
docker push kwraight/itk-pdb-template:cern
