import streamlit as st
import pandas as pd
import stInfrastructure as infra
### PDB stuff
import DBaccess

#####################
### useful functions
#####################


#####################
### main part
#####################

def main_part(state):
    ### get page name
    pageName=__file__.split('_')[-1].replace('.py','')
    ### title and (optional) instructions
    st.title(":clipboard:",pageName)
    st.write("---")
    st.write("## Register components to PDB")
    if state.debug:
        st.write("  * select *institution*")
        st.write("  * select *project*")
        st.write("  * get list of *componentTypes*")
        st.write("  * select *componentType*")
        st.write("  * get *componentType* schema")
        st.write("  * update *componentType* schema")
        st.write("  * review and upload")
        st.write("  * get *registerInfo* (if upload successful)")
    else:
        st.write(" * toggle debug for details")
    st.write("---")
    ###

    # debug check
    if state.debug:
        st.write("### Debug is on")

    ### add page attrubute to state
    # st.write([i for i in state.__dict__.keys() if i[:1] != '_'])
    if pageName in [i for i in state.__dict__.keys() if i[:1] != '_']:
        if state.debug: st.write("state."+pageName+" defined")
    else:
        state.__setattr__(pageName,{})
    # st.write([i for i in state.__dict__.keys() if i[:1] != '_'])

    ### getting attribute
    pageDict=state.__getattribute__(pageName)

    ### check requirements to do stuff
    doWork=False
    try:
        if state.myClient:
            doWork=True
        if state.debug:
            st.write(":white_check_mark: Got Token")
    except AttributeError:
        st.write("No token")

    ### gatekeeping
    if not doWork:
        st.stop()
